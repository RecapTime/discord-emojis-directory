# Contributing

## Scaping with DevTools

This might be the harder but usuing these source files are named in
some sort of UUID stuff.

## Scaping with DevTools Console

1. Open the Emoji picker on the target server you want to scrape source image URLs.

2. Open the DevTools Console, and paste the following snippet.

```js
// paste the following snippet below first
var emojiRaw = [];
document.querySelectorAll(".emojiListRow-m_GHp0 li").forEach((x) => {emojiRaw.push(x.children[0].src)});
var veryLongEmojiList =[...new Set(emojiRaw)];
// paste the following snippet above first

// then do this
console.log(veryLongEmojiList)
```

3. Copy the JSON content
